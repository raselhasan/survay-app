@extends('layout.app')
@section('style')

@endsection 
@section('content')
<div class="py-5 min-height-c px-3">

	<h4 class="text-center mb-4">Dėstytojo prisijungimas</h4>

	<div class="container-login bg-white p-4 mx-auto rounded">
		<form class="py-2" method="POST" action="{{ route('login') }}">
			@csrf
			<div class="mb-4">
				<label for="Email" class="form-label">El. paštas</label>
				<input 
					type="email" 
					class="form-control" 
					placeholder="Įrašykite savo el. pašto adresą"
					name="email" 
					value="{{ old('email') }}" 
					required 
					autocomplete="email" 
					autofocus
				>
				@error('email')
					<span class="v-error">{{ $message }}</span>
				@enderror
			</div>
			<div class="mb-4">
				<label for="Password" class="form-label">Pakartokite slaptažodį</label>
				<input 
					type="password" 
					class="form-control" 
					placeholder="Įveskite slaptažodį"
					name="password" 
					required 
					autocomplete="current-password"
				>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-primary d-inline-flex justify-content-center align-content-between">
					<span>Prisijungti</span> <i class="material-icons mr-1">arrow_forward</i>
				</button>
			</div> 
		</form>                
	</div>

	<p class="text-center mt-4">Neturite paskyros? <a href="{{url('/registration')}}">Registruokitės</a></p>
	
</div>
@endsection

@section('script')
@endsection