<script>
    window.base_url = '<?= url("/") ?>';
    window.token = '<?= csrf_token(); ?>';
    window.delete_subject = '<?= route("deleteSubject"); ?>';
    window.delete_university = '<?= route("deleteUniversity"); ?>';
    window.get_subject = '<?= route("getSubjects"); ?>';
    window.survay_filter = '<?= route("survayFilter"); ?>';
    window.survay_delete = '<?= route("deleteSurvay"); ?>';
    window.add_subject = '<?= route("addSubject"); ?>';
    window.add_university = '<?= route("addUniversity"); ?>';
</script>