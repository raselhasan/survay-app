<div class="container-footer py-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<h3 class="pb-2">Logotipas</h3>
				<p style="max-width: 272px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut commodo gravida nulla, et cursus erat faucibus eu.</p>
			</div>
			<div class="col-md-8 col-sm-12">
				<h5 class="pb-3">Nuorodos</h5>
				<ul class="list-group list-group-flush">
					<li class="list-group-item border-0 p-0 mb-3"><a href="{{url('/contact')}}" class="">Kontaktai</a></li>
					<li class="list-group-item border-0 p-0"><a href="{{url('/privacy')}}" class="">Privatumo politika</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>