 <nav class="navbar sticky-top navbar-dark navbar-expand-lg px-3">
 	<div class="container container-navbar">
 		<a class="navbar-brand" href="{{url('/')}}">Logotipas</a>
 		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
 			<span class="navbar-toggler-icon"></span>
 		</button>
 		<div class="collapse navbar-collapse" id="navbarScroll">
 			<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
			 	@if(auth()->user())
					<li class="nav-item">
						<a href="{{route('allSurvay')}}" class="btn btn-dark d-inline-flex justify-content-center align-content-between" (click)="refresh()" [disabled]="loading">
							<i class="material-icons mr-1">home</i>
							<span>Valdymo skydas</span>
						</a>
					</li>
				@endif

			 	@if(auth()->user())
					<li class="nav-item">
						<a href="{{route('newPoll')}}" class="btn btn-dark d-inline-flex justify-content-center align-content-between" (click)="refresh()" [disabled]="loading">
							<i class="material-icons mr-1">add</i>
							<span>Kurti nauja apklausa</span>
						</a>
					</li>
				@endif
				
				@if(auth()->user())
					<li class="nav-item">
						<a href="{{route('myAccount')}}" class="btn btn-dark d-inline-flex justify-content-center align-content-between" (click)="refresh()" [disabled]="loading">
							@if(auth()->user()->avatar)
 								<img src="{{ asset('avatar/'.auth()->user()->avatar) }}" style="width:25px; border-radius:100%; margin-right:3px" />
							@else
								<i class="material-icons mr-1">person</i>
							@endif
							<span>{{auth()->user()->name}}</span>
						</a>
					</li>
				@endif
				
				@if(auth()->user())
					<li class="nav-item">
						<a 
							class="nav-link" 
							href="{{ route('logout') }}" 
							onclick="event.preventDefault();
                        	document.getElementById('logout-form').submit();"
						>Atsijungti</a>
					</li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
						@csrf
					</form>
				@endif
 			</ul>
 		</div>
 	</div>
 </nav>