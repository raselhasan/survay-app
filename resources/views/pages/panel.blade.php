@extends('layout.app')
@section('style')

@endsection
@section('content')
<div class="min-height-c pb-5 pt-4">

	<div class="container px-3 px-md-0">

		<div class="border-bottom pb-3">
			<div class="d-md-flex align-items-center justify-content-between">
				<h4 class="mb-3 mb-md-0 d-flex align-items-center justify-content-between">Valdymo skydas <span class="d-block d-md-none" style="color: #707070;">Filtrai</span></h4>
				<div>
					<div class="d-md-flex align-items-center">
						<h5 class="me-4 mb-0 d-none d-md-block" style="color: #707070;">Filtrai</h5>
						<div class="d-flex align-items-center me-md-4 mb-3 mb-md-0">
							<h5 class="me-2 mb-0 fil-label">Universitetas</h5>
							<select class="form-select survay-university" id="survay-subject">
								<option value="">Visi</option>
								@if(count($universities) > 0)
									@foreach($universities as $university)
										<option value="{{$university->id}}">{{$university->name}}</option>
									@endforeach
								@endif
							</select> 
						</div>
						<div class="d-flex align-items-center">
							<h5 class="me-2 mb-0 fil-label">Dalykas</h5>
							<select class="form-select survay-subject">
								<option value="">Visi</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">Nr.</th>
						<th scope="col">Universitetas</th>
						<th scope="col">Dalykas</th>
						<th scope="col">Įdomumas</th>
						<th scope="col">Atmosfera</th>
						<th scope="col">Tempas</th>
						<th scope="col">Data</th>
						<th scope="col"></th>
					</tr>
				</thead>
				
				<tbody id="all-survay-list">
					@if(count($survays) > 0)
						@foreach($survays as $survay)
							<tr>
								<td><a href="{{route('viewSarvay', ['id'=>$survay->id])}}">#{{@$survay->poll->poll_number}}</a></td>
								<td>{{@$survay->poll->university->name}}</td>
								<td>{{@$survay->poll->subject->name}}</td>
								<td>
									@if($survay->curiosity || $survay->curiosity === 0)
										<span class="main-yellow">{{$survay->curiosity}}</span>/100
									@else 
										-
									@endif
								</td>
								<td>
									@if($survay->atmosphere || $survay->atmosphere === 0)
										<span class="main-green">{{$survay->atmosphere}}</span>/100
									@else 
										-
									@endif
								</td>
								<td>
									@if($survay->tempo || $survay->tempo === 0)
										<span class="main-red">{{$survay->tempo}}</span>/100
									@else 
										-
									@endif
								</td>
								<td>{{date('Y-m-d', strtotime($survay->created_at))}}</td>
								<td>
									<input type="text" id="survay-link" value="{{Request::root().'/'.@$survay->poll->link}}" style="display:none">
									<button onclick="copyLink('survay-link')" type="button" class="btn btn-silver d-inline-flex justify-content-center align-content-between btn-sm text-nowrap">
										<i class="material-icons mr-16">content_copy</i> Kopijuoti nuorodą</button>
								</td>
							</tr>
						@endforeach
					@endif		
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endsection

	@section('script')
	@endsection
