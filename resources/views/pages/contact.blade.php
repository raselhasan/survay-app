@extends('layout.app')
@section('style')

@endsection
@section('content')

<div class="py-5 min-height-c px-3">
	<h3 class="text-center mb-4">Kontaktai</h3>

	<div class="container-register bg-white p-4 mx-auto rounded">
		<form class="py-2">
			<div class="mb-4">
				<label for="Name" class="form-label">Vardas</label>
				<input type="text" class="form-control" placeholder="Įveskite savo vardą">
			</div>
			<div class="mb-4">
				<label for="Email" class="form-label">El. paštas</label>
				<input type="email" class="form-control" placeholder="Įrašykite savo el. pašto adresą">
			</div>
			<div class="mb-4">
				<label for="Komentaras" class="form-label">Komentaras</label>
				<textarea style="height: 100px; resize: none;" class="form-control p-3" placeholder="Parašyk savo komentarą"></textarea>
			</div>


			<div class="text-center mt-4">
				<button type="button" class="btn btn-primary d-inline-flex justify-content-center px-5">
					Pateikti
				</button>
			</div>
		</form>                
	</div>

</div>
@endsection

@section('script')
@endsection
