@extends('layout.app')
@section('style')

@endsection
@section('content')
<div class="py-5">
	<div class="container px-3 px-md-0">

		<ul class="nav nav-pills mb-4 pb-2 flex-nowrap" id="pills-tab" role="tablist">
			<li class="nav-item me-3" role="presentation">
				<button class="nav-link @if(!Session::has('tab')) active @endif" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Mano paskyra</button>
			</li>
			<li class="nav-item" role="presentation">
				<button class="nav-link @if(Session::has('tab')) active @endif" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Universitetai ir dalykai</button>
			</li>
		</ul> 

		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade @if(!Session::has('tab')) show active @endif" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
				<form class="p-md-2" method="POST" action="{{ route('updateProfile') }}" enctype="multipart/form-data">
					@csrf
					<div class="container-settings bg-white p-4">
						<div class="row">
							<div class="col-12 col-md-6">
								<h4 class="mb-3">Mano informacija</h4>
								<div class="mb-4">
									<label for="Name" class="form-label">Vardas</label>
									<input 
									type="text" 
									class="form-control" 
									placeholder="Įveskite savo vardą"
									name="name"
									value="{{ old('name') ? old('name') : auth()->user()->name }}" 
									required 
									autofocus
									>
								</div>
								<div class="mb-4">
									<label for="Surname" class="form-label">Pavardė</label>
									<input 
									type="text" 
									class="form-control" 
									placeholder="Įveskite savo pavardę"
									name="surname"
									value="{{ old('surname') ? old('surname') : auth()->user()->surname }}" 
									>
								</div>
								<div class="mb-4">
									<label for="Password" class="form-label">Profilio nuotrauka</label>
									<div class="d-flex">
										<div class="img-preview me-3">
											@php 
											$avatar = "http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png";
											if(auth()->user()->avatar)
											$avatar = asset('avatar/'.auth()->user()->avatar);
											
											@endphp
											<img 
											style="" 
											id="image-preview" 
											src="{{ $avatar }} " 
											alt="your image" 
											title=''
											/>
										</div>
										<div class="upload-btn-wrapper">
											<button class="btn">Pasirinkti naują nuotrauką</button>
											<input type="file" name="avatar" id="file-input" />
											<br/>
											@error('avatar')
											<span class="v-error">{{ $message }}</span>
											@enderror
										</div>
									</div>
								</div>          
							</div>
							<div class="col-12 col-md-6">
								<h4 class="mb-3">Slaptažodžio keitimas</h4>
								<div class="mb-4">
									<label for="Password" class="form-label">Salptažodis</label>
									<input 
									type="password" 
									class="form-control" 
									id="exampleInputPassword1"
									name="password"  
									autocomplete="new-password"
									>
									@error('password')
									<span class="v-error">{{ $message }}</span>
									@enderror
								</div>
								<div class="mb-4">
									<label for="Password" class="form-label">Pakartokite slaptažodį</label>
									<input 
									type="password" 
									class="form-control" 
									id="exampleInputPassword1"
									name="password_confirmation"  
									autocomplete="new-password"
									>
								</div>
							</div>
						</div>
					</div>
					<div class="text-end pt-4" style="max-width: 720px;">
						<button type="submit" class="btn btn-primary" >
							Išsaugoti
						</button>
					</div>
				</form>
				
			</div>

			<div class="tab-pane fade @if(Session::has('tab')) show active @endif" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
				<div class="row">
					<div class="col-md-6 mb-3 mb-md-0">
						<div class="bg-white container-settings">
							<div class="d-flex align-items-center mb-3">
								<h4 class="mb-0 me-3">Universitetai</h4>
								<a data-bs-toggle="modal" data-bs-target="#universityModal" class="d-inline-flex justify-content-center align-items-center curser-p">
									<i class="material-icons mr-1">add</i> <span>Pridėti universitetą</span>
								</a>
							</div>
							<span id="add-new-uni">
								@include('pages.components.university-list')
							<span>

						</div>
					</div>

					<div class="col-md-6">
						<div class="bg-white container-settings">
							<div class="d-flex align-items-center mb-3">
								<h4 class="mb-0 me-3">Dalykai</h4>
								<a data-bs-toggle="modal" data-bs-target="#subjectModal" class="d-inline-flex justify-content-center align-items-center curser-p">
									<i class="material-icons mr-1">add</i> <span>Pridėti dalyką</span>
								</a>
							</div>
							<span id="add-new-sub">
								@include('pages.components.subject-list')
							</span>	
							

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@include('pages.components.add-university')
@include('pages.components.add-subject')


@endsection

@section('script')
@endsection
