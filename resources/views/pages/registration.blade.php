@extends('layout.app')
@section('style')

@endsection
@section('content')
<div class="py-5 min-height-c px-3">
	<h4 class="text-center mb-4">Dėstytojo registracija</h4>

	<div class="container-register bg-white p-4 mx-auto rounded">
		<form class="py-2" method="POST" action="{{ route('register') }}" enctype="multipart/form-data" >
			@csrf
			<div class="row">
				<div class="col-md-6">
					<div class="mb-4">
						<label for="Name" class="form-label">Vardas</label>
						<input 
							type="text" 
							class="form-control" 
							placeholder="Įveskite savo vardą"
							name="name" 
							value="{{ old('name') }}" 
							required 
							autocomplete="name" 
							autofocus
						/>
						@error('name')
							<span class="v-error">{{ $message }}</span>
						@enderror					
					</div>
					<div class="mb-4">
						<label for="Surname" class="form-label">Pavardė</label>
						<input 
							type="text" 
							class="form-control" 
							placeholder="Įveskite savo pavardę"
							name="surname" 
							value="{{ old('surname') }}"  
							autocomplete="surname"
						/>
						@error('surname')
							<span class="v-error">{{ $message }}</span>
						@enderror
					</div>
					<div class="mb-4">
						<label for="Email" class="form-label">El. paštas</label>
						<input 
							type="email" 
							class="form-control" 
							placeholder="Įrašykite savo el. pašto adresą"
							name="email" 
							value="{{ old('email') }}" 
							required 
							autocomplete="email"
						/>
						@error('email')
							<span class="v-error">{{ $message }}</span>
						@enderror
					</div>
					<div class="mb-4">
						<label for="University" class="form-label">Universitetas</label>
						<input 
							type="text" 
							class="form-control" 
							placeholder="Įrašykite universiteto pavadinimą"
							name="university" 
							value="{{ old('university') }}"  
							autocomplete="university"
							required
						/>
						@error('university')
							<span class="v-error">{{ $message }}</span>
						@enderror
					</div>             
				</div>

				<div class="col-md-6">
					<div class="mb-4">
						<label for="Password" class="form-label">Salptažodis</label>
						<input 
							type="password" 
							class="form-control" 
							id="exampleInputPassword1"
							name="password" 
							required 
							autocomplete="new-password"
						/>
						@error('password')
							<span class="v-error">{{ $message }}</span>
						@enderror
						
					</div>
					<div class="mb-4">
						<label for="Password" class="form-label">Pakartokite slaptažodį</label>
						<input 
							type="password" 
							class="form-control" 
							id="exampleInputPassword1"
							name="password_confirmation" 
							required 
							autocomplete="new-password"
						>
					</div>
					<div class="mb-4">
						<label for="Password" class="form-label">Profilio nuotrauka</label>
						<div class="d-flex">
							<div class="img-preview me-3">
								<img style="display: none;" id="image-preview" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" title=''/>
							</div>
							<div class="upload-btn-wrapper">
								<button class="btn">Pasirinkti nuotrauką</button>
								<input type="file" name="avatar" id="file-input" />
							</div>
							
						</div>
						@error('avatar')
							<span class="v-error">{{ $message }}</span>
						@enderror
					</div>
					
					<div class="form-check" style="margin-top:15px">
						<input class="form-check-input" type="checkbox" value="1" id="flexCheckDefault" name="terms_condition" required>
						<label class="form-check-label" for="flexCheckDefault">
							Sutinku su <a href="#">privatumo politika</a>
						</label>
					</div>
					<div class="form-check" style="margin-top:15px">
						<input class="form-check-input" type="checkbox" value="2" id="flexCheckDefault" name="rules" required>
						<label class="form-check-label" for="flexCheckDefault">
							Sutinku su <a href="#">Taisyklėmis</a>
						</label>
					</div>
					
					
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-primary d-inline-flex justify-content-center align-content-between" (click)="refresh()" [disabled]="loading">
						<span>Registracija</span> <i class="material-icons mr-1">arrow_forward</i>
					</button>
				</div>
			</div>
		</form>                
	</div>
	<p class="text-center mt-4">Jau turite paskyrą?  <a href="{{url('/login')}}">Prisijunkite</a></p>

</div>
@endsection

@section('script')
@endsection
