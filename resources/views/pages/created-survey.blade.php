@extends('layout.app')
@section('style')

@endsection
@section('content')
<div class="py-5 min-height-c px-3">
	<div class="container-sucess bg-white p-4 mx-auto rounded">
		<h4 class="pb-4 mb-4 border-bottom"><span class="main-blue">#{{$poll->poll_number}}</span> apklausa sėkmingai sukurta.</h4>
		<div class="row mb-4">
			<div class="col-12 col-md-5">
				<div class="mb-1 font-14">Universiteto pavadinimas</div>
				<div class="mb-1 font-14">Dalyko pavadinimas</div>
			</div>
			<div class="col-12 col-md-7">
				<div class="mb-1 font-14">{{@$poll->university->name}}</div>
				<div class="mb-1 font-14">{{@$poll->subject->name}}</div>
			</div>
		</div>
		<div class="mb-4">
			<label for="Data" class="form-label">Apklausos nuoroda</label>
			<div class="d-md-flex">
				<input type="text" id="survay-link2" class="form-control mb-3 mb-md-0" name="data" value="{{Request::root().'/'.$poll->link}}">
				<button onclick="copyLink('survay-link2')" type="button" class="btn btn-primary d-inline-flex justify-content-center align-items-center ms-md-3 text-nowrap">
					<i class="material-icons mr-16">content_copy</i> Kopijuoti nuorodą</button>
				</div>
			</div>            
		</div>
		<div class="container-sucess mx-auto mt-4 p-0">
			<div class="text-center">
				<a href="{{route('newPoll')}}" class="btn btn-gray btn-transparent d-inline-flex justify-content-center align-content-between p-0">
					<i class="material-icons mr-1">arrow_back</i> <span>Grįžti į valdymo skydą</span>
				</a>
			</div>
		</div>

	</div>
	@endsection

	@section('script')
	@endsection
