@if(count($survays) > 0)
    @foreach($survays as $survay)
        <tr>
            <td><a href="{{route('viewSarvay', ['id'=>$survay->id])}}">#{{@$survay->poll->poll_number}}</a></td>
            <td>{{@$survay->poll->university->name}}</td>
            <td>{{@$survay->poll->subject->name}}</td>
            <td><span class="main-yellow">{{$survay->curiosity}}</span>/100</td>
            <td><span class="main-green">{{$survay->atmosphere}}</span>/100</td>
            <td><span class="main-red">{{$survay->tempo}}</span>/100</td>
            <td>{{date('Y-m-d', strtotime($survay->created_at))}}</td>
            <td>
                <button onclick="copyLink('survay-link')" type="button" class="btn btn-silver d-inline-flex justify-content-center align-content-between btn-sm text-nowrap">
                    <i class="material-icons mr-16">content_copy</i> Kopijuoti nuorodą</button>
            </td>
        </tr>
    @endforeach
@endif 