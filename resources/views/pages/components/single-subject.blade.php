<ul id="subj-{{$subject->id}}" class="list" style="margin-bottom: 5px !important;">
    <li class="d-flex align-items-center justify-content-between">
        <div class="me-2">
            <div class="light-gray">{{@$subject->university->name}}</div>
            <div class="color-black">{{$subject->name}}</div>
        </div> 
        <button onclick="deleteSubject('{{$subject->id}}')" type="button" class="btn d-inline-flex justify-content-center align-content-between btn-sm light-gray p-0">
            <i class="material-icons mr-1">remove</i> <span>Pašalinti</span>
        </button> 
    </li>
</ul> 