<!-- Modal -->
<form method="POST" action="{{route('addSubject')}}" id="addSubjectForm">
    @csrf
    <div class="modal fade" id="subjectModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="subjectModalLabel">Pridėti dalyką</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="mb-4">
                                <label for="Name" class="form-label">Universitetą pavadinimas</label>
                                <select name="university_id" class="form-control put-all-univer" id="exampleFormControlSelect1" required>
                                    @if(count($universities) > 0)
                                        @foreach($universities as $university)   
                                            <option value="{{$university->id}}">{{$university->name}}</option>
                                        @endforeach
                                    @endif  
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-12">
                            <div class="mb-4">
                                <label for="Name" class="form-label">Dalyką pavadinimas</label>
                                <input 
                                    type="text" 
                                    class="form-control sub-name" 
                                    placeholder="dalyką pavadinimas"
                                    name="name"
                                    value="" 
                                    required 
                                >
                            </div>
                        </div>
                    </div>        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>