@if($domClass == 'poll-subject')
    <option value="">Pasirinkite dalyką</option>
@endif
@if($domClass == 'survay-subject')
    <option value="">Visi</option>
@endif       
@if(count($subjects) > 0)
    @foreach($subjects as $subject)
        <option value="{{$subject->id}}">{{$subject->name}}</option>
    @endforeach
@endif    
