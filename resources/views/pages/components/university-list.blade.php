@if(count($universities) > 0)
    @foreach($universities as $university)
        <ul id="univ-{{$university->id}}" class="list" style="margin-bottom: 5px !important;">
            <li class="d-flex align-items-center justify-content-between">
                <div class="me-2">
                    <div class="">{{$university->name}}</div>
                </div> 
                <button onclick="deleteUniversity('{{$university->id}}')" type="button" class="btn d-inline-flex justify-content-center align-content-between btn-sm light-gray p-0">
                    <i class="material-icons mr-1">remove</i> <span>Pašalinti</span>
                </button> 
            </li>
        </ul> 
    @endforeach
@endif        