@extends('layout.app')
@section('style')

@endsection
@section('content')
<div class="py-5 min-height-c px-3">
	<form method="POST" action="{{route('addPoll')}}">
		@csrf 
		<h4 class="text-center mb-4">Naujos apklausos kūrimas</h4>
		<div class="container-login bg-white p-4 mx-auto rounded">
			<div class="py-2">
				<div class="mb-4">
					<label for="Email" class="form-label">Universitetas</label>
					<select class="form-select" required id="poll-subject" name="university_id">
						<option value="">Pasirinkite universitetą</option>
						@if(count($universities) > 0)
						@foreach($universities as $university)
						<option value="{{$university->id}}">{{$university->name}}</option>
						@endforeach
						@endif		
					</select>
				</div>
				<div class="mb-4">
					<label for="Email" class="form-label">Dalykas</label>
					<select class="form-select poll-subject" required name="subject_id">
						<option value="0">Pasirinkite dalyką</option>
					</select>
				</div>
				<div class="mb-4">
					<label for="Data" class="form-label">Data</label>
					<input type="date" class="form-control" name="date" placeholder="Nurodykite paskaitos laiką" required>
				</div>
			</div>                
		</div>
		<div class="container-login mx-auto mt-4">
			<div class="text-center">
				<button type="submit" class="btn btn-primary d-inline-flex justify-content-center align-items-center">
					<span>Sukurti</span> <i class="material-icons ms-1">arrow_forward</i>
				</button>
			</div>
		</div>
	</form>
</div>
@endsection

@section('script')
@endsection
