@extends('layout.app')
@section('style')

@endsection
@section('content')
<div class="py-5 px-3">
	<form action="{{route('addSurvay', ['poll_id'=>$poll->id])}}" method="POST">
		@csrf
		@php 
		$avatar = "http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png";
		if(@$poll->user->avatar)
		$avatar = asset('avatar/'.$poll->user->avatar);
		@endphp
		<div class="container shadow-sm bg-body rounded container-survery mb-4">
			<div class="d-md-flex align-items-center">
				<div class="survery-image mb-3 mb-md-0">
					<img src="{{$avatar}}" alt="Dėstytojo profilio nuotrauka" class="cover">
				</div>
				<div class="ms-md-3">
					<h2>{{@$poll->user->name}} {{@$poll->user->surname}}</h2>
					<!-- <p class="light-gray">
						<span class="dark-gray">Universiteto pavadinimas</span> &#183;
						<span class="dark-gray">Dalyko pavadinimas</span> &#183; <span class="light-gray">Vykusios paskaitos laikas: </span><span class="dark-gray">Gruodžio 21, 2021</span>
					</p> -->
					<nav style="--bs-breadcrumb-divider: '';" aria-label="breadcrumb">
						<ol class="breadcrumb mb-0">
							<li class="breadcrumb-item dark-gray">{{@$poll->university->name}}</li>
							<li class="breadcrumb-item dark-gray" aria-current="page">Vykusios paskaitos laikas: {{date('M d Y', strtotime($poll->poll_date))}}</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
		<div class="container container-info">
			<p class="light-gray mb-4">Donec consectetur nisl laoreet nisi tempus, eu varius neque mattis. Fusce imperdiet ligula in orci rutrum mattis. Vestibulum ut tortor aliquam, ornare nulla vitae, convallis nisl. Integer vel ultrices nulla, et bibendum enim. Nunc pulvinar tortor
			faucibus mattis aliquam. Aenean dignissim dui et metus scelerisque, eget fringilla felis aliquam.</p>
		</div>
		<!-- Survey -->
		<div class="container shadow-sm bg-body rounded container-question">
			<h3 class="mb-4">Paskaitoje man buvo įdomu (paskaitos įdomumas)</h3>
			<div class="d-flex justify-content-center">
				<div class="">
					<img class="emotion-image" src="{{asset('assets/imgs/emotion-1.png')}}">
				</div>
				<div class="slider-center px-3">
					<!-- <div class="position-relative">
						<span class="range-lr range-l"></span>
						<span class="range-lr range-r"></span>
						<div class="range1 mb-3"></div>
						<input type="hidden" id="curiosity" name="curiosity" value="50">
					</div> -->
					<input type="range" class="form-range" min="0" max="100" step="1" id="customRange3" name="curiosity">
					<div class="row gx-0">
						<div class="col-6"><img class="img-fluid img-arrow" src="{{asset('assets/imgs/triange-1.svg')}}"></div>
						<div class="col-6"><img class="img-fluid img-arrow" src="{{asset('assets/imgs/triange-2.svg')}}"></div>
					</div>
				</div>
				<div class="">
					<img class="emotion-image" src="{{asset('assets/imgs/emotion-2.png')}}">
				</div>
			</div>
		</div>

		<div class="container shadow-sm bg-body rounded container-question">
			<h3 class="mb-4">Paskaitoje jaučiausi gerai (paskaitos atmosfera)</h3>
			<div class="d-flex justify-content-center">
				<div class="">
					<img class="emotion-image" src="{{asset('assets/imgs/emotion-3.png')}}">
				</div>
				<div class="slider-center px-3">
					<!-- <div class="position-relative">
						<span class="range-lr range-l"></span>
						<span class="range-lr range-r"></span>
						<div class="range2 mb-3"></div>
						<input type="hidden" id="atmosphere" name="atmosphere" value="50">
					</div> -->
					<input type="range" class="form-range" min="0" max="100" step="1" id="customRange3" name="atmosphere">
					<div class="row gx-0">
						<div class="col-6"><img class="img-fluid img-arrow" src="{{asset('assets/imgs/triange-1.svg')}}"></div>
						<div class="col-6"><img class="img-fluid img-arrow" src="{{asset('assets/imgs/triange-2.svg')}}"></div>
					</div>
				</div>

				<div class="">
					<img class="emotion-image" src="{{asset('assets/imgs/emotion-4.png')}}">
				</div>
			</div>
		</div>

		<div class="container shadow-sm bg-body rounded container-question">
			<h3 class="mb-4">Spėjau įsisavinti informaciją (paskaitos tempas)</h3>
			<div class="d-flex justify-content-center">
				<div class="">
					<img class="emotion-image" src="{{asset('assets/imgs/emotion-5.png')}}">
				</div>
				<div class="slider-center px-3">
					<!-- <div class="position-relative">
						<span class="range-lr range-l"></span>
						<span class="range-lr range-r"></span>
						<div class="range3 mb-3"></div>
						<input type="hidden" id="tempo" name="tempo" value="50">
					</div> -->
					<input type="range" class="form-range" min="0" max="100" step="1" id="customRange3" name="tempo">
					<div class="row gx-0">
						<div class="col-6"><img class="img-fluid img-arrow" src="{{asset('assets/imgs/triange-1.svg')}}"></div>
						<div class="col-6"><img class="img-fluid img-arrow" src="{{asset('assets/imgs/triange-2.svg')}}"></div>
					</div>
				</div>
				<div class="">
					<img class="emotion-image" src="{{asset('assets/imgs/emotion-6.png')}}">
				</div>
			</div>
		</div>

		<!-- Send button -->

		<div class="text-center">
			<button class="btn btn-primary" type="submit">Siųsti</button>
		</div>
	</form>

</div>
@endsection

@section('script')
<!-- <script src="{{ asset('assets/range/custom-range.js') }}"></script> -->
@endsection
