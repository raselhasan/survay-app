@extends('layout.app')
@section('style')

@endsection
@section('content')
<div class="py-5 min-height-c px-3">
	<div class="container-sucess bg-white p-4 mx-auto rounded">
		<h4 class="pb-4 mb-4 border-bottom">Dėkojame, jūsų atsakymas sėkmingai išsiųstas</h4>
		<div class="row mb-4">
			<div class="col-12 col-md-5">
				<div class="mb-1 font-14">Universiteto pavadinimas</div>
				<div class="mb-1 font-14">Dalyko pavadinimas</div>
			</div>
			<div class="col-12 col-md-7">
				<div class="mb-1 font-14">{{@$poll->university->name}}</div>
				<div class="mb-1 font-14">{{@$poll->subject->name}}</div>
			</div>
		</div>
	</div>
	<div class="container-sucess mx-auto mt-4 p-0">
		<div class="text-center">
			<a href="{{url('/')}}" class="btn btn-gray btn-transparent d-inline-flex justify-content-center align-content-between p-0">
				<span>Galite uždaryti puslapį</span>
			</a>
		</div>
	</div>

</div>
@endsection

@section('script')
@endsection
