@extends('layout.app')
@section('style')

@endsection
@section('content')
<div class="py-5 min-height-c">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 mb-4 mb-md-0">
				<h4 class="mb-3">Duis sollicitudin id urna nec feugiat. Sed</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pharetra lobortis tellus sed posuere. Sed lorem urna, tempor vel lobortis ac, vehicula quis mauris. Nunc nunc eros, ullamcorper non diam sit amet, consequat pretium nisi. Nullam ac ornare urna. Aenean fringilla ultrices congue. Duis sollicitudin id urna nec feugiat. Sed interdum velit non ante tincidunt consequat. Mauris et lacinia arcu. Nullam gravida vitae justo at euismod. Donec nec ligula purus. Aliquam nec leo tortor.</p>
				@if(!auth()->check())
					<a class=" me-3" href="{{url('/registration')}}">
						<button type="button" class="btn btn-primary">Registracija
						</button>
					</a>
					<a href="{{url('/login')}}">
						<button type="button" class="btn btn-outline-primary">Prisijungimas</button>
					</a>
				@endif
			</div>                
			<div class="col-12 col-md-6">
				<div class="home-d bg-grey d-flex align-items-center justify-content-center w-100 h-100">
					<h4 class="color-black mb-0">Nuotrauka</h4>
				</div>                
			</div>                
		</div>
	</div>
</div>
@endsection

@section('script')

@endsection
