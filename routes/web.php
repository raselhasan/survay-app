<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\IndexController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UniversityController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\SurvayController;
use App\Http\Controllers\PollController;
use App\Http\Controllers\HomeController;

Route::get('/', [IndexController::class, 'index'])->name('home');
Route::get('/registration', [IndexController::class, 'registration'])->name('registration');

Route::get('/contact', [IndexController::class, 'contact']);
Route::get('/privacy', [IndexController::class, 'privacy']);

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [SurvayController::class, 'allSurvay'])->name('allSurvay');
    Route::post('/filter-dashboard', [SurvayController::class, 'survayFilter'])->name('survayFilter');

    Route::get('/view-servay/{id}', [SurvayController::class, 'viewSarvay'])->name('viewSarvay');
    Route::get('/delete-survay/{id?}', [SurvayController::class, 'deleteSurvay'])->name('deleteSurvay');

    Route::get('/my-account', [IndexController::class, 'myAccount'])->name('myAccount');
    Route::post('/my-account', [ProfileController::class, 'updateProfile'])->name('updateProfile');

    Route::post('/add-university', [UniversityController::class, 'addUniversity'])->name('addUniversity');
    Route::post('/delete-university', [UniversityController::class, 'deleteUniversity'])->name('deleteUniversity');
    Route::post('/add-subject', [SubjectController::class, 'addSubject'])->name('addSubject');
    Route::post('/delete-subject', [SubjectController::class, 'deleteSubject'])->name('deleteSubject');
    Route::post('/get-subjects', [SubjectController::class, 'getSubjects'])->name('getSubjects');

    Route::get('/new-poll', [IndexController::class, 'newPoll'])->name('newPoll');
    Route::post('/add-poll', [PollController::class, 'addPoll'])->name('addPoll');
    Route::get('/created-survey/{id}', [PollController::class, 'pollSuccess'])->name('pollSuccess');

    
    
});


Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('/add-survey/{poll_id}', [SurvayController::class, 'addSurvay'])->name('addSurvay');
Route::get('/survey-answer/{poll_id}', [SurvayController::class, 'surveyAnswer'])->name('survey-answer');
Route::get('/{link}', [SurvayController::class, 'index'])->name('survay');


Route::fallback(function () {
    Route::redirect('/');
});
