function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#image-preview').attr('src', e.target.result);
      $('#image-preview').show();
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#file-input").change(function() {
  readURL(this);
});


function deleteSubject(id)
{
  $.confirm({
    title: 'Alert!',
    content: 'Are you sure to delete this item?',
    buttons: {
      confirm: function () {
        var data = new FormData();
        data.append('id',id);
        data.append('_token',window.token);
        $.ajax({
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url: window.delete_subject,
          success: function(response) {
            $('#subj-'+id).remove();
            toastr["success"]("Deleted!");
          }
        });
      },
      cancel: function () {

      },
    }
  });
}


function deleteUniversity(id)
{
  $.confirm({
    title: 'Alert!',
    content: 'Are you sure to delete this item?',
    buttons: {
      confirm: function () {
        var data = new FormData();
        data.append('id',id);
        data.append('_token',window.token);
        $.ajax({
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url: window.delete_university,
          success: function(response) {
            $('#univ-'+id).remove();
            toastr["success"]("Deleted!");
          }
        });
      },
      cancel: function () {

      },
    }
  });
}

function getSubjects( id, domClass )
{
  var data = new FormData();
  data.append('id',id);
  data.append('domClass', domClass)
  data.append('_token',window.token);
  $.ajax({
    processData: false,
    contentType: false,
    data: data,
    type: 'POST',
    url: window.get_subject,
    success: function(response) {
      $('.'+domClass).html(response);
    }
  });
}

$(document).on('change','#poll-subject', function(){
  var id = $('#poll-subject option').filter(':selected').val();
  (id == 0) ? $('.poll-subject').html('<option value="">Pasirinkite dalyką</option>') : getSubjects(id, 'poll-subject');
});

$(document).on('change','#survay-subject', function(){
  var id = $('#survay-subject option').filter(':selected').val();
  (id == 0) ? $('.survay-subject').html('<option pollvalue="">Visi</option>') : getSubjects(id, 'survay-subject');
});

function survayFilter( university_id, subject_id){
  var data = new FormData();
  data.append('university_id',university_id);
  data.append('subject_id', subject_id)
  data.append('_token',window.token);
  $.ajax({
    processData: false,
    contentType: false,
    data: data,
    type: 'POST',
    url: window.survay_filter,
    success: function(response) {
      //console.log(response)
      $('#all-survay-list').html(response);
    }
  });
}

$(document).on('change','.survay-university',function(){
  var university_id = $('.survay-university').val();
  var subject_id = $('.survay-subject').val();
  if(university_id == 'Visi')
    university_id = '';
  if(subject_id == 'Visi')
    subject_id = '';   
  survayFilter(university_id, subject_id)
  
})

$(document).on('change','.survay-subject',function(){
  var university_id = $('.survay-university').val();
  var subject_id = $('.survay-subject').val();
  if(university_id == 'Visi')
    university_id = '';
  if(subject_id == 'Visi')
    subject_id = '';  
  survayFilter(university_id, subject_id)
})

function copyLink( id )
{
  var $temp = $("<input>");
  var $url = $('#'+id).val();
  
  $("body").append($temp);
  $temp.val($url).select();
  document.execCommand("copy");
  $temp.remove();
  toastr["success"]("Link copied!")
}


function deleteSurvay(id)
{
  $.confirm({
    title: 'Alert!',
    content: 'Are you sure to delete this item?',
    buttons: {
      confirm: function () {
        var url = window.survay_delete+'/'+id;
        window.location.replace(url);
      },
      cancel: function () {

      },
    }
  });
}


$(document).on('submit','#addUniversityForm',function(e){
  e.preventDefault();
  var formdata = new FormData($('#addUniversityForm')[0]);
  $.ajax({
    processData: false,
    contentType: false,
    data: formdata,
    type: 'POST',
    url: window.add_university,
    success: function(response) {
      if(response.error)
        toastr["error"](response.error)
      if(response.success){
        $('#add-new-uni').prepend(response.data)
        toastr["success"](response.success)
        $('#universityModal').modal('hide');
        $('.put-all-univer').prepend(response.allUni)
        $('.univ-name').val('');

      }
    }
  });
})


$(document).on('submit','#addSubjectForm',function(e){
  e.preventDefault();
  var formdata = new FormData($('#addSubjectForm')[0]);
  $.ajax({
    processData: false,
    contentType: false,
    data: formdata,
    type: 'POST',
    url: window.add_subject,
    success: function(response) {
      if(response.error)
        toastr["error"](response.error)
      if(response.success){
        $('#add-new-sub').prepend(response.data)
        toastr["success"](response.success)
        $('#subjectModal').modal('hide');
        $('.sub-name').val('');
      }
    }
  });
})

