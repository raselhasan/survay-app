$(".range1").wRunner({
	step: 1,
	type: "single",
	limits: {
		minLimit: 0,
		maxLimit: 100
	},
	singleValue: $('#curiosity').val(),
	rangeValue: {
		minValue: 0,
		maxValue: 100
	},
	roots: document.body,
	divisionsCount: 0,
	valueNoteDisplay: true,
	theme: "default",
	direction: 'horizontal',
});
$(".range2").wRunner({
	step: 1,
	type: "single",
	limits: {
		minLimit: 0,
		maxLimit: 100
	},
	singleValue: $('#atmosphere').val(),
	rangeValue: {
		minValue: 0,
		maxValue: 100
	},
	roots: document.body,
	divisionsCount: 0,
	valueNoteDisplay: true,
	theme: "default",
	direction: 'horizontal',
});
$(".range3").wRunner({
	step: 1,
	type: "single",
	limits: {
		minLimit: 0,
		maxLimit: 100
	},
	singleValue: $('#curiosity').val(),
	rangeValue: {
		minValue: 0,
		maxValue: 100
	},
	roots: document.body,
	divisionsCount: 0,
	valueNoteDisplay: true,
	theme: "default",
	direction: 'horizontal',
});
