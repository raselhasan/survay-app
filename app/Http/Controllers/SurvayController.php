<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Poll;
use App\Models\Survay;
use App\Models\University;

class SurvayController extends Controller
{
    public function index( $link )
    {
        $data['poll'] = Poll::where('link',$link)->with('university','subject','user')->first();
        return view('pages.survey', $data);
    }

    public function addSurvay( Request $request, $poll_id )
    {
        $poll = Poll::findOrFail($poll_id);
        $survay = new Survay();
        $survay->poll_id = $poll_id;
        $survay->curiosity = $request->curiosity;
        $survay->atmosphere = $request->atmosphere;
        $survay->tempo = $request->tempo;
        $survay->university_id = $poll->university_id;
        $survay->subject_id = $poll->subject_id;
        $survay->user_id = $poll->user_id;
        $survay->save();
        return redirect()->route('survey-answer',['poll_id'=>$poll_id]);
    }

    public function surveyAnswer( $poll_id )
    {
        $data['poll'] = Poll::where('id',$poll_id)->with('university','subject','user')->first();
        return view('pages.survey-answer', $data);
    }

    public function allSurvay()
    {
        $data['survays'] = Survay::where('user_id',auth()->user()->id)->orderBy('id','desc')->with('poll','poll.subject','poll.university','poll.user')->get();
        $data['universities'] = University::where('user_id',auth()->user()->id)->orderBy('id','desc')->get();
        return view('pages.panel', $data);
    }

    public function survayFilter( Request $request )
    {
        $university_id = $request->university_id;
        $subject_id = $request->subject_id;
        if($university_id != '' && $subject_id != '')
            $data['survays'] = Survay::where('user_id',auth()->user()->id)
            ->where('university_id', $university_id)
            ->where('subject_id', $subject_id)
            ->with('poll','poll.subject','poll.university','poll.user')
            ->orderBy('id','desc')
            ->get();
        if($university_id != '' && $subject_id == '')
            $data['survays'] = Survay::where('user_id',auth()->user()->id)
            ->where('university_id', $university_id)
            ->with('poll','poll.subject','poll.university','poll.user')
            ->orderBy('id','desc')
            ->get();
        if($university_id == '')
            $data['survays'] = Survay::where('user_id',auth()->user()->id)
            ->with('poll','poll.subject','poll.university','poll.user')
            ->orderBy('id','desc')
            ->get();
    
        return view('pages.components.survay-list', $data)->render();        
    }

    public function viewSarvay( $id )
    {
        $data['survay'] = Survay::where('id',$id)->where('user_id',auth()->user()->id)->with('poll','poll.subject','poll.university','poll.user')->first();
        return view('pages.view-survay', $data);
    }
    public function deleteSurvay( $id=null )
    {
        Survay::where('id',$id)->where('user_id',auth()->user()->id)->delete();
        return redirect()->route('allSurvay')->with(['success'=>'deleted!']);
    }
}
