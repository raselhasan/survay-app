<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function addSubject( Request $request )
    {
        if($this->hasSubject($request->name, $request->university_id))
            return response()->json(['error'=>'Subject already exist!']);

        $subject = new Subject();
        $subject->university_id = $request->university_id;
        $subject->name = $request->name;
        $subject->user_id = auth()->user()->id;
        $subject->save();

        $subject = Subject::where('id',$subject->id)->with('university')->first();

        $html = view('pages.components.single-subject',compact('subject'))->render();
        return response()->json(['data'=>$html, 'success'=>'saved!']);


        //return redirect()->back()->with(['success'=>'Subject successfully saved', 'tab'=>'2']);
    }

    public function deleteSubject( Request $request )
    {
        Subject::where('id', $request->id)->delete();
        return redirect()->back()->with(['success'=>'Subject successfully deleted', 'tab'=>'2']);
    }

    public function getSubjects(Request $request)
    {
        $data['domClass'] = $request->domClass;
        $data['subjects'] = Subject::where('university_id', $request->id)->orderBy('id','desc')->get();
        return view('pages.components.subject-dropdown', $data)->render();
    }
    public function hasSubject( $name, $university_id )
    {
        $upper = strtoupper(trim($name));
        $lower = strtolower(trim($name));
        $sub =  Subject::where('university_id',$university_id)->where('name',$name)
            ->first();
        if(!$sub)
            $sub = Subject::where('university_id',$university_id)->where('name',$upper)
                ->first();
        if(!$sub)
            $sub = Subject::where('university_id',$university_id)->where('name',$lower)
                    ->first();
        return $sub;                    
    }
}
