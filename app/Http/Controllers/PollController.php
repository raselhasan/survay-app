<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Poll;
use App\Models\Survay;

class PollController extends Controller
{
    public function link(){
        return Str::random(11);
    }
    public function pollNumber()
    {
        $pollNumber = '000001';
        $findNumber = Poll::where('poll_number', '!=', null)->orderBy('id','desc')->first();
        if($findNumber){
            $pollNumber = str_pad(intval($findNumber->poll_number) + 1, strlen($findNumber->poll_number), '0', STR_PAD_LEFT);
        }
        return $pollNumber;
    }
    public function addPoll( Request $request )
    {   
        $hasPoll = Poll::where('university_id',$request->university_id)
            ->where('subject_id', $request->subject_id)
            ->first();
        if($hasPoll)
            return redirect()->route('pollSuccess',['id'=>$hasPoll->id]);
        $poll = new Poll();
        $poll->university_id = $request->university_id;
        $poll->subject_id = $request->subject_id;
        $poll->poll_date = $request->date;
        $poll->poll_number = $this->pollNumber();
        $poll->link = $this->link();
        $poll->user_id = auth()->user()->id;
        $poll->save();

        $survay = new Survay();
        $survay->poll_id = $poll->id;
        $survay->university_id = $poll->university_id;
        $survay->subject_id = $poll->subject_id;
        $survay->user_id = $poll->user_id;
        $survay->save();

        return redirect()->route('pollSuccess',['id'=>$poll->id]);
    }

    public function pollSuccess( $id )
    {
        $data['poll'] = Poll::where('id',$id)->with('university','subject')->first();
        return view('pages.created-survey', $data);
    }
}
