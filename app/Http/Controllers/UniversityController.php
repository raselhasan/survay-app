<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\University;

class UniversityController extends Controller
{
    public function addUniversity( Request $request )
    {
        if($this->hasUniversity($request->name))
            return response()->json(['error'=>'University already exist!']);
        $university = new University();
        $university->name = $request->name; 
        $university->user_id = auth()->user()->id; 
        $university->save();

        $allUni = view('pages.components.university-option',compact('university'))->render();
        $html = view('pages.components.single-university',compact('university'))->render();
        return response()->json(['data'=>$html, 'success'=>'saved!', 'allUni'=>$allUni]);

        //return redirect()->back()->with(['success'=>'University successfully saved', 'tab'=>'2']);
    }

    public function deleteUniversity( Request $request )
    {
        University::where('id', $request->id)->delete();
        return redirect()->back()->with(['success'=>'University successfully deleted', 'tab'=>'2']);
    }

    public function hasUniversity( $name )
    {
        $upper = strtoupper(trim($name));
        $lower = strtolower(trim($name));
        $uni = University::where('user_id', auth()->user()->id)
            ->where('name',$name)
            ->first();

        if(!$uni)
            $uni = University::where('user_id', auth()->user()->id)
            ->where('name',$lower)
            ->first();

        if(!$uni)
            $uni = University::where('user_id', auth()->user()->id)
            ->where('name',$upper)
            ->first();
        return $uni; 
    }
}
