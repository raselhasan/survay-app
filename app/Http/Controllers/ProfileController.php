<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function updateProfile( Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'password' => 'nullable|string|min:8|confirmed',
            'avatar' => 'nullable|image|mimes:png,jpeg,gif,jpg',
        ]);
        $user  =  User::find(auth()->user()->id);
        $user->name = $request->name;
        if($request->password) $user->password = Hash::make($request->password);
        $user->surname = $request->surname;

        if(request()->hasFile('avatar')){
            $image = request()->file('avatar');
            $fileName = time().$image->getClientOriginalName();
            $image->move(public_path('/avatar'), $fileName);
            if(auth()->user()->avatar){
                $file = public_path('avatar/'.auth()->user()->avatar);
                if(file_exists($file))
                    unlink($file);
            }
            $user->avatar = $fileName;
        }
        $user->save();
        return redirect()->back()->with(['success'=>'Profile successfully updated']);
    }
}
