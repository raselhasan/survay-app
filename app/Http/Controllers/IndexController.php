<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\University;
use App\Models\Subject;

class IndexController extends Controller
{
	public function index(){
		return view('pages.index');
	}
	public function contact(){
		return view('pages.contact');
	}
	public function privacy(){
		return view('pages.privacy');
	}
	public function login(){
		return view('pages.login');
	}
	public function registration(){
		return view('pages.registration');
	}
	public function panel(){
		return view('pages.panel');
	}
	public function myAccount(){
		$data['universities'] = University::where('user_id',auth()->user()->id)->orderBy('id','desc')->get();
		$data['subjects'] = Subject::where('user_id',auth()->user()->id)->with('university')->orderBy('id','desc')->get();
		return view('pages.my-account', $data);
	}
	public function newPoll(){
		$data['universities'] = University::where('user_id',auth()->user()->id)->orderBy('id','desc')->get();
		return view('pages.new-poll', $data);
	}
	public function createdSurvey(){
		return view('pages.created-survey');
	}
	public function surveyAnswer(){
		return view('pages.survey-answer');
	}
	public function survey(){
		return view('pages.survey');
	}
}
