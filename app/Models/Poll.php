<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    use HasFactory;

    protected $fillable = [
        'university_id',
        'subject_id',
        'poll_number',
        'link',
        'poll_date',
        'user_id'
    ];  

    public function university()
    {
        return $this->belongsTo(University::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
}
