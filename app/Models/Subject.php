<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    protected $fillable = ['university_id', 'name', 'user_id'];


    public function university()
    {
        return $this->belongsTo(University::class);
    }
}
