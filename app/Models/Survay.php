<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survay extends Model
{
    use HasFactory;

    protected $fillable = [
        'university_id',
        'subject_id',
        'poll_id',
        'curiosity',
        'atmosphere',
        'tempo',
        'user_id'
    ];

    public function poll(){
        return $this->belongsTo(Poll::class);
    }
}
